<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Repositories\UserRepository;
use PhPermission\Validation\MakePinValidation;
use PhPermission\Validation\VerifyPinValidation;

/**
 * Class PinController
 * @package PhPermission\Controllers
 */
class PinController extends Controller
{

    /**
     * @return mixed
     * @throws \Exception
     */
    public function makePin()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new MakePinValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::setPin($params->user_id, $params->pin);

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'pin has done!',
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function verifyPin()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new VerifyPinValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        if (!UserRepository::verifyPin($params->user_id, $params->pin)) {
            return $this
                ->response
                ->setPayloadError('pin is invalid');
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'Success!',
            ]);
    }
}