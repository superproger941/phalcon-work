{{ content() }}

<div>

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Update user</h2>
    </div>

    <table class="signup updateUser">
        <tr>
            <td align="right">{{ form.label('username') }}</td>
            <td>
                {{ form.render('username') }}
                {{ form.messages('username') }}
            </td>
        </tr>
        <tr>
            <td align="right">{{ form.label('email') }}</td>
            <td>
                {{ form.render('email') }}
                {{ form.messages('email') }}
            </td>
        </tr>
        <tr>
            <td align="right">{{ form.label('authMethod') }}</td>
            <td>
                {{ form.render('authMethod') }}
                {{ form.messages('authMethod') }}
            </td>
        </tr>

        <tr>
            <td align="right">{{ form.label('role') }}</td>
            <td>
                {{ form.render('role') }}
                {{ form.messages('role') }}
            </td>
        </tr>

        <tr>
            <td align="right"></td>
            <td>{{ form.render('Send') }}</td>
        </tr>
    </table>

    {{ form.render('csrf', ['value': security.getToken()]) }}
    {{ form.messages('csrf') }}

    <hr>

    {{ end_form() }}

</div>