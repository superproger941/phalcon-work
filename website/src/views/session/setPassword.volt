{{ content() }}

<div align="center">

	{{ form('class': 'form-search') }}

	<div align="left">
		<h2>Change password</h2>
	</div>

	<table class="signup changePass">
		<tr>
			<td align="right">{{ form.label('password') }}</td>
			<td>
				{{ form.render('password') }}
				{{ form.messages('password') }}
			</td>
		</tr>
		<tr>
			<td align="right">{{ form.label('confirmPassword') }}</td>
			<td>
				{{ form.render('confirmPassword') }}
				{{ form.messages('confirmPassword') }}
			</td>
		</tr>
		<tr>
			<td align="right">{{ form.label('newPassword') }}</td>
			<td>
				{{ form.render('newPassword') }}
				{{ form.messages('newPassword') }}
			</td>
		</tr>
		<tr>
			<td align="right"></td>
			<td>{{ form.render('Send') }}</td>
		</tr>
	</table>

	{{ form.render('csrf', ['value': security.getToken()]) }}
	{{ form.messages('csrf') }}

	<hr>

	</form>

</div>