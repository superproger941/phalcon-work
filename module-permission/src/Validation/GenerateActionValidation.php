<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

class GenerateActionValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfActionName = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field action name is required',
            ]
        );
        $this->add('action_name', $presenceOfActionName);

        $presenceOfAuthMethods = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth methods is required',
            ]
        );
        $this->add('auth_methods', $presenceOfAuthMethods);
    }
}
