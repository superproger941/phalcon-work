<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class UserAuthMethodModel
 * @package PhPermission\Models
 */
class UserAuthMethodModel extends Model
{
    /**
     *
     * @var string
     * @Column(column="user_id", type="integer", length=32, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(column="auth_method_id", type="integer", length=32, nullable=false)
     */
    public $auth_method_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('users_auth_methods');

        $this->belongsTo(
            'auth_method_id',
            'PhPermission\Models\AuthMethodModel',
            'id',
            [
                'alias'    => 'AuthMethod',
            ]
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource(): string
    {
        return 'users_auth_methods';
    }

    /**
     * @return array
     */
    public static function allowedFields(): array
    {
        return [
            'user_id',
            'auth_method_id',
        ];
    }

    /**
     * @throws \Exception
     */
    public function afterFetch()
    {
        if(!$this->authMethod->active) {
            throw new \Exception('Auth method deactivate. Please call in support.');
        }
    }
}
