<?php

namespace ServiceClient\Core;

/**
 * Interface ResponseInterface
 * @package ServiceClient\Core
 */
interface ResponseInterface
{
    /**
     * @return array|null
     */
    public function toArray(): ?array;

    /**
     * @return object
     */
    public function toObject(): object;

    /**
     * @param $d
     * @return mixed
     */
    public function arrayToObject($d);

    /**
     * @return bool
     */
    public function hasError(): bool;

    /**
     * @return array
     */
    public function getError(): array;

    /**
     * @return string
     */
    public function gerErrorMessage(): string;

}