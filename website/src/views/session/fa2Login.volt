{{ content() }}

<div align="center" class="well">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>2fa log In</h2>
    </div>

    {{ form.label('code') }}
    {{ form.render('code') }}
    {{ form.messages('code') }}


    {{ form.render('csrf', ['value': security.getToken()]) }}

    <div>
        {{ form.render('Send') }}
    </div>

    </form>

</div>