<?php

declare(strict_types=1);

namespace PhUser\Providers;

use PhUser\Guard\AuthGuard;
use PhUser\User\UserProvider;
use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class AuthProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        /**
         * Authorization
         * @return AuthGuard
         */
        $container->setShared('auth', function (){
            return new AuthGuard(new UserProvider());
        });
    }
}
