<?php

namespace MWS\Swagger\User\Controllers;

use MWS\Swagger\MainController;

/**
 * Class MultiFactorAuthController
 * @package MWS\Swagger\User\Controllers
 */
class MultiFactorAuthController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Verify 2fa code",
     *     operationId="verify2fa",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="multiFactorAuth.verify2fa"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="code",
     *                             type="string",
     *                             example="534835"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="Success!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function verify2fa()
    {
        //user_id
        //code
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Get qr code",
     *     operationId="getQrCode",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="multiFactorAuth.getQrCode"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="code",
     *                             type="string",
     *                             example="123456"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function getQrCode()
    {
        //user_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Do 2fa auth",
     *     operationId="do2fa",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="multiFactorAuth.do2fa"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="code",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="action_name",
     *                             type="string",
     *                             example="doLogin"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="success!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function do2fa()
    {
        //code
        //auth_session_token
        //action_name
    }
}