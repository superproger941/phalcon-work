<?php

namespace MWS\Provider;

use Phalcon\Mvc\View as ViewEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

/**
 * MWS\Provider\View\ServiceProvider
 *
 * @package MWS\Provider\View
 */
class View extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'view';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $config = container('config')->application;

                $view = new ViewEngine();

                $view->setViewsDir($config->viewsDir);

                $view->registerEngines([
                    '.volt' => function ($view) use($config) {

                        $volt = new VoltEngine($view, $this);

                        $volt->setOptions([
                            'compiledPath' => $config->cacheDir . 'volt/',
                            'compiledSeparator' => '_'
                        ]);

                        return $volt;
                    }
                ]);

                return $view;
            }
        );
    }
}
