<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Validation;

class Send2faValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfPublicKey = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field public_key is required',
            ]
        );
        $this->add('public_key', $presenceOfPublicKey);

    }
}
