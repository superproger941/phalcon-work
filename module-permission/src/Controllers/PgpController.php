<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Repositories\UserRepository;
use PhPermission\Services\PgpService;
use PhPermission\Validation\GetEncryptHashValidation;
use PhPermission\Validation\MakePGPValidation;
use PhPermission\Validation\VerifyDecryptHashValidation;

/**
 * Class PgpController
 * @property PgpService
 * @package PhPermission\Controllers
 */
class PgpController extends Controller
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public function makePGP()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new MakePGPValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $hash = md5(rand());
        try {
            $encryptedString = $this->pgp->generatePGP($params->public_key, $hash);
        } catch (\Exception $e) {
            return $this->response->setPayloadError($e->getMessage());
        }
        UserRepository::setPgp($params->user_id, $encryptedString, $hash);

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'Public key has saved!'
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getEncryptHash()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new GetEncryptHashValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = UserRepository::retrieveById($params->user_id);
        $pgp = $user->pgp;

        return $this
            ->response
            ->setPayloadSuccess([
                'encrypt_hash' => $pgp->pgp_encrypt_hash ?? '',
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function verifyDecryptHash()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new VerifyDecryptHashValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        if (!UserRepository::verifyPgpHash($params->user_id, $params->decrypt_hash)) {
            return $this
                ->response
                ->setPayloadError('invalid decrypt hash string');
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'Success!',
            ]);
    }
}