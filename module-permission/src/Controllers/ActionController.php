<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Repositories\UserRepository;
use PhPermission\Validation\CheckActionCompleteValidation;
use PhPermission\Validation\GenerateActionValidation;
use PhPermission\Validation\GetUserIdAuthSessionTokenValidation;
use PhPermission\Validation\SetupAuthMethodValidation;

/**
 * Class ActionController
 * @package PhPermission\Controllers
 */
class ActionController extends Controller
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public function generate()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new GenerateActionValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::retrieveById($params->user_id);
        $authSessionToken = $this->action->generateAuthSessionToken($params->user_id);
        $action = $this->action->generate($params->user_id, $params->action_name, $params->auth_methods, $authSessionToken);

        return $this
            ->response
            ->setPayloadSuccess([
                'auth_session_token' => $authSessionToken,
                'action' => $action
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function completeAuthMethod()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new SetupAuthMethodValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::retrieveById($params->user_id);
        $this->action->setPassActionAuthMethod($params->user_id, $params->action_name, $params->auth_method, $params->auth_session_token);

        return $this
            ->response
            ->setPayloadSuccess([
                'action' => $this->action->getAction($params->user_id, $params->action_name, $params->auth_session_token)
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setActionComplete()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new CheckActionCompleteValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::retrieveById($params->user_id);
        $complete = $this->action->checkActionComplete($params->user_id, $params->action_name, $params->auth_session_token);

        if($complete) {
            $this->action->dropAuthSessionToken($params['auth_session_token']);
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'complete' => $complete
            ]);
    }

    /**
     * @return mixed
     */
    public function getUserIdByAuthSessionToken()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new GetUserIdAuthSessionTokenValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $userId = $this->action->getUserIdByAuthSessionToken($params['auth_session_token']);

        return $this
            ->response
            ->setPayloadSuccess(['user_id' => $userId]);
    }

}