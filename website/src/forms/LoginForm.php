<?php
namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Filter;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form
{
    use ApiValidation;

    public function check()
    {
        if ( ! $this->isPostAndValid()) {
            return false;
        }

        $params = [
            'username' => $this->request->getPost('username', 'striptags'),
            'password' => $this->request->getPost('password'),
        ];

        $res = $this->userService->send('auth.doLogin', $params);

        if ($res->hasError()) {
            $error = $res->getError();
            $this->flash->error($error['message'] ?? 'Unknown error');
            if ( ! empty($error['fields'])) {
                $this->setMessages($error['fields']);
            }

            return false;
        }

        $this->session->destroy();
        $this->session->start();

        if (isset($res->data['auth_session_token']) && isset($res->data['action'])) {
            $arr = [
                'auth_session_token' => $res->data['auth_session_token'],
                'action' => $res->data['action'],
                'user_id' => $res->data['user_id']
            ];
            $this->session->set('auth-session', $arr);
            $this->response->redirect('/');

            return false;
        }

        $this->flashSession->success('Welcome back user');
        $this->session->set('auth', [
            'id' => $res->data['id'],
            'username' => $res->data['username'],
            'token' => $res->data['token']
        ]);

        return true;
    }

    public function initialize()
    {
        // Email
        $username = new Text('username', [
            'placeholder' => 'Username'
        ]);

        $username->addValidators([
            new PresenceOf([
                'message' => 'The username is required'
            ]),
            /*new Email([
                'message' => 'The e-mail is not valid'
            ])*/
        ]);

        $username->setFilters(
            [
                Filter::FILTER_ALPHANUM,
                Filter::FILTER_TRIM,
            ]
        );

        $this->add($username);

        // Password
        $password = new Password('password', [
            'placeholder' => 'Password'
        ]);

        $password->addValidator(new PresenceOf([
            'message' => 'The password is required'
        ]));

        $password->clear();

        $this->add($password);

        // Remember
        $remember = new Check('remember', [
            'value' => 'yes'
        ]);

        $remember->setLabel('Remember me');

        $this->add($remember);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'Form expired. Please reload this page.'
        ]));

        $csrf->clear();

        $this->add($csrf);

        $this->add(new Submit('go', [
            'class' => 'btn btn-success'
        ]));
    }
}
