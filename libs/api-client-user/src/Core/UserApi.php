<?php

namespace ServiceClientUser\Core;

use ServiceClient\Core\Api;

/**
 * Class UserApi
 * @package ServiceClientUser\Core
 */
class UserApi extends Api
{
    /**
     * UserApi constructor.
     * @param array $context
     */
    public function __construct(array $context = [])
    {
        parent::__construct($context);
        $this->baseUrl = !empty(getenv('MODULE_USER_ADDR')) ? getenv('MODULE_USER_ADDR') : 'http://localhost/user/call';
    }

}