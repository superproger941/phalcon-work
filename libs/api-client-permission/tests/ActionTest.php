<?php

namespace Tests;

use Carbon\Carbon;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\Purpose;
use PHPUnit\Framework\TestCase;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class ActionTest
 * @package Tests
 */
class ActionTest extends TestCase
{
    /**
     * @return Builder
     * @throws \ParagonIE\Paseto\Exception\InvalidKeyException
     * @throws \ParagonIE\Paseto\Exception\InvalidPurposeException
     * @throws \ParagonIE\Paseto\Exception\PasetoException
     */
    public function testGetTokenBuilder(): Builder
    {
        $key = SymmetricKey::fromEncodedString('tD2ONiqbfxcOLK9JLT6BHsQ2C8UR8BC2cs6t0t3tBcO');
        $builder = (new Builder)
            ->setKey($key)
            ->setVersion(new Version2)
            ->setPurpose(Purpose::local());
        $this->assertEquals(
            true,
            true
        );

        return $builder;
    }

    /**
     * @depends testGetTokenBuilder
     * @param Builder $builder
     * @return PermissionService
     */
    public function testConstructor(Builder $builder): PermissionService
    {
        $arr['id'] = 5;
        $arr['username'] = 'userTest1';
        $api = new PermissionApi($arr);
        $permissionService = new PermissionService($api);
        $token = $builder
            ->setExpiration(Carbon::now()->addHours(24))
            ->setIssuer('api')
            ->setClaims([]);
        $permissionService->setAuthToken($token);
        $this->assertEquals(
            true,
            true
        );

        return $permissionService;
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testGenerateAction(PermissionService $permissionService): \ServiceClient\Core\ResponseInterface
    {
        try {
            $response = $permissionService->send('action.generate', [
                'user_id' => 5,
                'auth_methods' => [
                    '2fa',
                    'pgp'
                ],
                'action_name' => 'doLogin'
            ]);
        }
        catch (\Exception $e) {
            echo $e->getTraceAsString();
        }
        $this->assertEquals(
            true,
            isset($response->data['auth_session_token']) && isset($response->data['action'])
        );

        return $response;
    }

    /**
     * @depends testConstructor
     * @depends testGenerateAction
     * @param PermissionService $permissionService
     * @param $response
     * @throws \Exception
     */
    public function testSetupAuthMethod(PermissionService $permissionService, $response)
    {
        $authSessionToken = $response->data['auth_session_token'];
        $response = $permissionService->send('action.completeAuthMethod', [
            'user_id' => 5,
            'auth_method' => '2fa',
            'action_name' => 'doLogin',
            'auth_session_token' => $authSessionToken
        ]);

        $this->assertEquals(
            true,
            isset($response->data['action']) &&
            isset($response->data['action']['auth_methods']['2fa']) &&
            $response->data['action']['auth_methods']['2fa'] === true
        );
    }

    /**
     * @depends testConstructor
     * @depends testGenerateAction
     * @param $permissionService
     * @param $response
     * @throws \Exception
     */
    public function testErrorSetupAuthMethod(PermissionService $permissionService, $response)
    {
        $authSessionToken = $response->data['auth_session_token'];
        $response = $permissionService->send('action.completeAuthMethod', [
            'userId' => 0,
            'auth_method' => '2fa',
            'action_name' => 'doLogin',
            'auth_session_token' => $authSessionToken
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @depends testGenerateAction
     * @param PermissionService $permissionService
     * @param $response
     * @throws \Exception
     */
    public function testGetUserIdByAuthSessionToken(PermissionService $permissionService, $response)
    {
        $authSessionToken = $response->data['auth_session_token'];
        $response = $permissionService->send('action.getUserIdByAuthSessionToken', [
            'auth_session_token' => $authSessionToken
        ]);

        $this->assertEquals(
            true,
            isset($response->data['user_id'])
        );
    }

    /**
     * @depends testConstructor
     * @param $permissionService
     * @throws \Exception
     */
    public function testErrorGetUserIdByAuthSessionToken(PermissionService $permissionService)
    {
        $response = $permissionService->send('action.getUserIdByAuthSessionToken', [
            'auth_session_token' => 'wrong',
            'action_name' => 'doLogin',
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @depends testGenerateAction
     * @param PermissionService $permissionService
     * @param $response
     * @throws \Exception
     */
    public function testSetActionComplete(PermissionService $permissionService, $response)
    {
        $authSessionToken = $response->data['auth_session_token'];
        $response = $permissionService->send('action.setActionComplete', [
            'user_id' => 5,
            'auth_session_token' => $authSessionToken,
            'action_name' => 'doLogin'
        ]);

        $this->assertEquals(
            true,
            isset($response->data['complete']) && $response->data['complete'] === true
        );
    }

    /**
     * @depends testConstructor
     * @depends testGenerateAction
     * @param PermissionService $permissionService
     * @param $response
     * @throws \Exception
     */
    public function testErrorSetActionComplete(PermissionService $permissionService, $response)
    {
        $response = $permissionService->send('action.setActionComplete', [
            'user_id' => 0,
            'auth_session_token' => 'wrong',
            'action_name' => 'doLogin'
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }
}