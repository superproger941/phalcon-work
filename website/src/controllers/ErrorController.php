<?php
namespace MWS\Controllers;

use Phalcon\Mvc\Controller;

/**
 * Display error pages.
 */
class ErrorController extends Controller
{

    /**
     * bad request action
     */
    public function route400Action()
    {
        return "bad request";
    }

    /**
     * not found action
     */
    public function route404Action()
    {
        return "page not found";
    }

    /**
     * server error action
     */
    public function route500Action()
    {
        return "internal server error";
    }
}
