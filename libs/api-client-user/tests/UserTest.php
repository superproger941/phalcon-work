<?php

namespace ServiceClientUserTests;

use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\Purpose;
use PHPUnit\Framework\TestCase;
use ServiceClient\Core\ResponseInterface;
use ServiceClientUser\Core\UserApi;
use ServiceClientUser\Services\UserService;

/**
 * Class UserTest
 * @package ServiceClientUserTests
 */
class UserTest extends TestCase
{
    /**
     * @return Builder
     * @throws \ParagonIE\Paseto\Exception\InvalidKeyException
     * @throws \ParagonIE\Paseto\Exception\InvalidPurposeException
     * @throws \ParagonIE\Paseto\Exception\PasetoException
     */
    public function testGetTokenBuilder(): Builder
    {
        $key = SymmetricKey::fromEncodedString('tD2ONiqbfxcOLK9JLT6BHsQ2C8UR8BC2cs6t0t3tBcO');
        $builder = (new Builder)
            ->setKey($key)
            ->setVersion(new Version2)
            ->setPurpose(Purpose::local());

        $this->assertEquals(
            true,
            true
        );

        return $builder;
    }

    /**
     * @return UserService
     */
    public function testConstructorWithoutToken(): UserService
    {
        $arr['id'] = 5;
        $arr['username'] = 'userTest1';
        $api = new UserApi($arr);
        $userService = new UserService($api);
        $this->assertEquals(
            true,
            true
        );

        return $userService;
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @return ResponseInterface
     * @throws \Exception
     */
    public function testFullDoLogin(UserService $userService): ResponseInterface
    {
        $response = $userService->send('auth.doLogin', [
            'username' => 'userThirdTest',
            'password' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            isset($response->data['id']) && isset($response->data['username']) && isset($response->data['token'])
        );

        return $response;
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @return ResponseInterface
     * @throws \Exception
     */
    public function testErrorFullDoLogin(UserService $userService): ResponseInterface
    {
        $response = $userService->send('auth.doLogin', [
            'username' => 'userThirdTestError',
            'password' => 'userTest'
        ]);

        $this->assertEquals(
            false,
            isset($response->data['id']) && isset($response->data['username']) && isset($response->data['token'])
        );

        return $response;
    }

    /**
     * @depends testFullDoLogin
     * @param $response
     * @return UserService
     */
    public function testConstructor($response): UserService
    {
        $arr['id'] = 5;
        $arr['username'] = 'userTest1';
        $api = new UserApi($arr);
        $userService = new UserService($api);
        $userService->setAuthToken($response->data['token']);
        $this->assertEquals(
            true,
            true
        );

        return $userService;
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @return ResponseInterface
     * @throws \Exception
     */
    public function testPartOfDoLogin(UserService $userService): ResponseInterface
    {
        $response = $userService->send('auth.doLogin', [
            'username' => 'userSecondTest',
            'password' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            isset($response->data['auth_session_token']) && isset($response->data['action'])
        );

        return $response;
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @return ResponseInterface
     * @throws \Exception
     */
    public function testErrorPartOfDoLogin(UserService $userService): ResponseInterface
    {
        $response = $userService->send('auth.doLogin', [
            'username' => 'userSecondTestError',
            'password' => 'userTest'
        ]);

        $this->assertEquals(
            false,
            isset($response->data['auth_session_token']) && isset($response->data['action'])
        );

        return $response;
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @return ResponseInterface
     * @throws \Exception
     */
    public function testPartOfDoLoginForPgp(UserService $userService): ResponseInterface
    {
        $response = $userService->send('auth.doLogin', [
            'username' => 'userPgpTest',
            'password' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            isset($response->data['auth_session_token']) && isset($response->data['action'])
        );

        return $response;
    }

    /**
     * @depends testConstructorWithoutToken
     * @depends testPartOfDoLoginForPgp
     * @param UserService $userService
     * @param $response
     * @throws \Exception
     */
    public function testDoFullPgpLogin(UserService $userService, $response)
    {
        $response = $userService->send('pgpAuth.doPGP', [
            'auth_session_token' => $response->data['auth_session_token'],
            'decoded_string' => 'testPgpString',
            'action_name' => 'doLogin'
        ]);

        $this->assertEquals(
            true,
            $response->data['action']['auth_methods']['pgp'] === true
        );
    }

    /**
     * @depends testConstructorWithoutToken
     * @depends testPartOfDoLoginForPgp
     * @param UserService $userService
     * @param $response
     * @throws \Exception
     */
    public function testErrorDoFullPgpLogin(UserService $userService, $response)
    {
        $response = $userService->send('pgpAuth.doPGP', [
            'auth_session_token' => $response->data['auth_session_token'],
            'decoded_string' => 'testError',
            'action_name' => 'doLogin'
        ]);

        $this->assertEquals(
            false,
            $response->data['action']['auth_methods']['pgp'] === true
        );
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @throws \Exception
     */
    public function testDoRegistration(UserService $userService)
    {
        $response = $userService->send('auth.doRegistration', [
            'username' => 'testman' . rand(),
            'password' => 'testman' . rand()
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'You are registered now. Please login'
        );
    }

    /**
     * @depends testConstructorWithoutToken
     * @param UserService $userService
     * @throws \Exception
     */
    public function testErrorDoRegistration(UserService $userService)
    {
        $response = $userService->send('auth.doRegistration', [
            'username' => '',
            'password' => 'testman' . rand()
        ]);

        $this->assertEquals(
            false,
            $response->data['message'] === 'You are registered now. Please login'
        );
    }


    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testSetPassword(UserService $userService)
    {
        $response = $userService->send('auth.changePassword', [
            'user_id' => 5,
            'password' => 'userTest',
            'new_password' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'Password has changed'
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testErrorSetPassword(UserService $userService)
    {
        $response = $userService->send('auth.changePassword', [
            'user_id' => 999,
            'password' => 'userTest',
            'new_password' => 'userTest'
        ]);

        $this->assertEquals(
            false,
            $response->data['message'] === 'Password has changed'
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testGetProfile(UserService $userService)
    {
        $response = $userService->send('user.profile', [
            'user_id' => 5
        ]);

        $this->assertEquals(
            true,
            empty($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testErrorGetProfile(UserService $userService)
    {
        $response = $userService->send('user.profile', [
            'user_id' => 999
        ]);

        $this->assertEquals(
            false,
            empty($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testUpdateProfile(UserService $userService)
    {
        $response = $userService->send('user.update', [
            'user_id' => 5,
            'username' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'user update!'
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testErrorUpdateProfile(UserService $userService)
    {
        $response = $userService->send('user.update', [
            'user_id' => 999,
            'username' => 'userTest'
        ]);

        $this->assertEquals(
            false,
            $response->data['message'] === 'user update!'
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testGetQrCode(UserService $userService)
    {
        $response = $userService->send('multiFactorAuth.getQrCode', [
            'user_id' => 5,
            'username' => 'userTest'
        ]);

        $this->assertEquals(
            true,
            isset($response->data['code'])
        );
    }

    /**
     * @depends testConstructor
     * @param UserService $userService
     * @throws \Exception
     */
    public function testErrorGetQrCode(UserService $userService)
    {
        $response = $userService->send('multiFactorAuth.getQrCode', [
            'user_id' => 999,
            'username' => 'userTest'
        ]);

        $this->assertEquals(
            false,
            isset($response->data['code'])
        );
    }

    /**
     * @depends testConstructorWithoutToken
     * @depends testPartOfDoLoginForPgp
     * @param UserService $userService
     * @param $response
     * @throws \Exception
     */
    public function testVerifyToken(UserService $userService, $response)
    {
        $response = $userService->send('auth.verifyToken', [
            'user_id' => 8,
            'auth_session_token' => $response->data['auth_session_token']
        ]);

        $this->assertEquals(
            true,
            isset($response->data['id']) && isset($response->data['username']) && isset($response->data['token'])
        );
    }

    /**
     * @depends testConstructorWithoutToken
     * @depends testPartOfDoLoginForPgp
     * @param UserService $userService
     * @param $response
     * @throws \Exception
     */
    public function testErrorVerifyToken(UserService $userService, $response)
    {
        $response = $userService->send('auth.verifyToken', [
            'user_id' => 999,
            'auth_session_token' => $response->data['auth_session_token']
        ]);

        $this->assertEquals(
            false,
            isset($response->data['id']) && isset($response->data['username']) && isset($response->data['token'])
        );
    }
}