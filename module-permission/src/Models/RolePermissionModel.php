<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class RolePermissionModel
 * @package PhPermission\Models
 */
class RolePermissionModel extends Model
{

    /**
     *
     * @var integer
     * @Column(column="role_id", type="integer", length=32, nullable=false)
     */
    public $role_id;

    /**
     *
     * @var integer
     * @Column(column="permission_id", type="integer", length=32, nullable=false)
     */
    public $permission_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('role_permissions');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'role_permissions';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'role_id',
            'permission_id'
        ];
    }
}
