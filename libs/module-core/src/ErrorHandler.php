<?php

declare(strict_types=1);

namespace PhCore;

use function memory_get_usage;
use function microtime;
use function number_format;
use Monolog\Logger;
use Phalcon\Config;
use PhCore\Http\Response;
use Psr\Log\LoggerInterface;
use Phalcon\Http\ResponseInterface;

/**
 * Class ErrorHandler
 *
 * @package PhCore
 */
class ErrorHandler
{
    /** @var Config */
    private $config;

    /** @var Logger */
    private $logger;

    /** @var Response */
    private $response;

    /**
     * ErrorHandler constructor.
     *
     * @param LoggerInterface $logger
     * @param Config $config
     * @param ResponseInterface $response
     */
    public function __construct(LoggerInterface $logger, Config $config, ResponseInterface $response)
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->response = $response;
    }

    /**
     * Handles errors by logging them
     *
     * @param int $number
     * @param string $message
     * @param string $file
     * @param int $line
     */
    public function handle(int $number, string $message, string $file = '', int $line = 0)
    {
        $str = sprintf(
            '[#:%s]-[L: %s] : %s (%s)',
            $number,
            $line,
            $message,
            $file
        );
        $this
            ->logger
            ->error($str);
        $this->response
            ->setPayloadError($str, Response::HTTP_INTERNAL_SERVER_ERROR)
            ->send();
        exit();
    }

    /**
     * Application shutdown - logs metrics in devMode
     */
    public function shutdown()
    {
        if (true === getenv("APP_DEBUG", false)) {
            $memory = number_format(memory_get_usage() / 1000000, 2);
            $execution = number_format(
                microtime(true) - $this->config->app->time,
                4
            );

            $this
                ->logger
                ->info(
                    sprintf(
                        'Shutdown completed [%s]s - [%s]MB',
                        $execution,
                        $memory
                    )
                );
        }
    }
}