<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class AuthMethodModel
 * @package PhPermission\Models
 */
class AuthMethodModel extends Model
{
    const AUTH_METHOD_DEFAULT = 'default';
    const AUTH_METHOD_2FA = '2fa';
    const AUTH_METHOD_PGP = 'pgp';
    const AUTH_METHOD_PIN = 'pin';

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", nullable=false)
     */
    public $name;

    /**
     *
     * @var boolean
     * @Column(column="active", type="boolean")
     */
    public $active;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('auth_methods');

        $this->hasMany('id', UserAuthMethodModel::class, 'auth_method_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'auth_methods';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'name',
            'active'
        ];
    }
}
