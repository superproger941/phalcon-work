<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\InclusionIn;

class AuthMethodActiveValidation extends Validation
{
    public function initialize()
    {
        $presenceOfAuthMethodId = new PresenceOf(
            [
                'message' => 'Field auth_method_id is required',
            ]
        );
        $this->add('auth_method_id', $presenceOfAuthMethodId);

        $presenceOfActive = new PresenceOf(
            [
                'message' => 'Field active is required',
            ]
        );
        $this->add('active', $presenceOfActive);

        $this->add(
            'active',
            new InclusionIn(
                [
                    'message' => 'The value must be 0 or 1',
                    'domain'  => [0, 1],
                ]
            )
        );
    }
}
