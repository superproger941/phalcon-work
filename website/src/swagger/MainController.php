<?php

namespace MWS\Swagger;

/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         version="1.0.0",
 *         title="Mozello apidoc",
 *         @OA\License(name="MIT")
 *     ),
 *     @OA\Server(
 *         description="Mozello api server",
 *         url="localhost",
 *     ),
 * )
 */
/**
 *  @OA\Schema(
 *      schema="Error",
 *      required={"code", "message"},
 *      @OA\Property(
 *          property="code",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @OA\Property(
 *          property="message",
 *          type="string"
 *      )
 *  ),
 *  @OA\Schema(
 *      schema="Permission",
 *      type="array",
 *      @OA\Items(ref="#/components/schemas/Permission")
 *  )
 */
class MainController
{
}