<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateRoleTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('roles', ['signed' => false]);
        $table->addColumn('name',  Literal::from('varchar'))
            ->create();
        $table = $this->table('user_roles', ['signed' => false]);
        $table->addColumn('user_id', 'integer')
            ->addColumn('role_id', 'integer')
            ->create();
        $table->changePrimaryKey(['user_id', 'role_id']);
        $table->removeColumn('id');
        $table->addForeignKey('role_id', 'roles');
        $table->save();
    }
}
