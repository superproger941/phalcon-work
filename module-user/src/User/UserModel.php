<?php

namespace PhUser\User;

use PhCore\Model\AbstractModel;
use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

/**
 * Class UserModel
 * @package PhUser\User\
 */
class UserModel extends AbstractModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="username", type="string", length=255, nullable=false)
     */
    public $username;

    /**
     *
     * @var string
     * @Column(column="password", type="string", length=255, nullable=false)
     */
    public $password;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('users');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'username'
        ];
    }

    /**
     * Model filters
     *
     * @return array<string,string,string>
     */
    public function getModelFilters(): array
    {
        return [
            'id' => Filter::FILTER_ABSINT,
            'username' => Filter::FILTER_STRING,
            'password' => Filter::FILTER_STRING
        ];
    }

    /**
     * Validates the username
     *
     * @return bool
     */
    public function validation()
    {
        $validator = new Validation();
        $validator->add(
            'username',
            new Uniqueness(
                [
                    'message' => 'The username already exists in the database',
                ]
            )
        );

        return $this->validate($validator);
    }
}
