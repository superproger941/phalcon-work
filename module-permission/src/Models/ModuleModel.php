<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class ModulesModel
 * @package PhPermission\Models
 */
class ModuleModel extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", nullable=false)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('modules');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'modules';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'name'
        ];
    }
}
