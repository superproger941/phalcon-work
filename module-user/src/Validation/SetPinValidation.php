<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class SetPinValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfPin = new PresenceOf(
            [
                'message' => 'pin is required',
            ]
        );
        $this->setFilters('pin', Filter::FILTER_STRIPTAGS);
        $this->add('pin', $presenceOfPin);
    }
}
