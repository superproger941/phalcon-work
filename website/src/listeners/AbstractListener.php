<?php

namespace MWS\Listener;

use Phalcon\DiInterface;
use Phalcon\Mvc\User\Component;

/**
 * MWS\Listener\AbstractListener
 *
 * @package MWS\Listener
 */
class AbstractListener extends Component
{
    //don't understand why he created that class and named it abstract
    public function __construct(DiInterface $di = null)
    {
        $di = $di ?: container();

        $this->setDI($di);
    }
}
