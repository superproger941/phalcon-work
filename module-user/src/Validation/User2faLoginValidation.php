<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Alnum;

class User2faLoginValidation extends Validation
{
    public function initialize()
    {
        $presenceOfSessionToken = new PresenceOf(
            [
                'message' => 'Have no auth_session_token. Please login again or go to our callcenter',
            ]
        );
        $this->setFilters('auth_session_token', Filter::FILTER_STRIPTAGS);
        $this->add('auth_session_token', $presenceOfSessionToken);


        $presenceOfActionName = new PresenceOf(
            [
                'message' => 'Field action_name is required',
            ]
        );
        $this->setFilters('action_name', Filter::FILTER_STRIPTAGS);
        $this->add('action_name', $presenceOfActionName);


        $presenceOf2faCode = new PresenceOf(
            [
                'message' => 'Field code is required',
            ]
        );
        $alnum2faCode = new Alnum(
            [
                'message' => 'Field code should have only numbers',
            ]
        );
        $this->setFilters('code', Filter::FILTER_STRIPTAGS);
        $this->add('code', $presenceOf2faCode);
        $this->add('code', $alnum2faCode);
    }
}
