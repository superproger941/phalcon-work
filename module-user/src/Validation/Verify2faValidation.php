<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Validation;

class Verify2faValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfCode = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field code is required',
            ]
        );
        $this->add('code', $presenceOfCode);

    }
}
