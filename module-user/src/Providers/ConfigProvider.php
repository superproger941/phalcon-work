<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'config', include BASE_DIR . '/config/config.php'
        );
    }
}
