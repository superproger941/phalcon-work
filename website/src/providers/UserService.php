<?php

namespace MWS\Provider;

use ServiceClientUser\Core\UserApi;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class UserService extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'userService';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $authIdentity = container('session')->get('auth')??[];
                $api = new UserApi($authIdentity);
                $service = new \ServiceClientUser\Services\UserService($api);
                $service->setAuthToken($authIdentity['token']??'');

                return $service;
            }
        );
    }
}
