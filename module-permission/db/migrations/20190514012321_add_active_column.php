<?php

use Phinx\Migration\AbstractMigration;

class AddActiveColumn extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('auth_methods');
        $table
            ->addColumn('active', 'boolean', ['default' => true])
            ->update();
    }
}
