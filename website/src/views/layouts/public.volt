<div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        {{ link_to(null, 'class': 'brand', 'Mozello')}}
        <div class="nav-collapse">
          <ul class="nav">

            {%- set menus = [
              'Home': 'index'
            ] -%}

            {% if session.get('auth') is not empty %}
                <?php $menus['Setup 2fa'] = 'profile/setup2fa' ?>
                <?php $menus['Setup pgp'] = 'profile/setupPgp' ?>
                <?php $menus['Setup pin'] = 'profile/setupPin' ?>
            {% endif %}

            {%- for key, value in menus %}
              {% if value === dispatcher.getControllerName() %}
              <li class="active">{{ link_to(value, key) }}</li>
              {% else %}
              <li>{{ link_to(value, key) }}</li>
              {% endif %}
            {%- endfor -%}

          </ul>

          <ul class="nav pull-right">

            {% if session.get('auth') is empty %}
                <li>{{ link_to('session/signup', 'Create Account') }}</li>
                <li>{{ link_to('session/login', 'Login') }}</li>
            {% else %}
                <li>{{ link_to('session/logout', 'Logout') }}</li>
            {% endif %}
          </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div>

<div class="container main-container">
  {{ flashSession.output() }}
  {{ content() }}
</div>

<footer>
Made with love by the Mozello Team

    {{ link_to("privacy", "Privacy Policy") }}
    {{ link_to("terms", "Terms") }}

© {{ date("Y") }} Mozello.
</footer>
