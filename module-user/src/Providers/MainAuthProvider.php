<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhUser\Components\AuthMethods\MainAuthComponent;
use PhUser\Components\AuthMethods\MultiFactorAuthComponent;
use PhUser\Components\PermissionComponent;
use PhUser\User\UserProvider;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class MainAuthProvider
 * @package PhUser\Providers
 */
class MainAuthProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared('mainAuth',
            function () use ($container) {
                $auth = $container->getShared('auth');
                return new MainAuthComponent($auth, new UserProvider());
            });
    }
}
