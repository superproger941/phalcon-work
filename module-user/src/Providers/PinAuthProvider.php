<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhUser\Components\AuthMethods\PinAuthComponent;
use PhUser\User\UserProvider;

/**
 * Class PinAuthProvider
 * @package PhUser\Providers
 */
class PinAuthProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared('pinAuth',
            function () use ($container) {
                $auth = $container->getShared('auth');

                return new PinAuthComponent($auth, new UserProvider());
            });
    }
}
