<?php

namespace PhPermission\Exceptions;

/**
 * Class MultifactorNotFoundException
 * @package PhPermission\Exceptions
 */
class MultifactorNotFoundException extends \Exception
{
}

