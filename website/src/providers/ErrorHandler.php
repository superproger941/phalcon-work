<?php

namespace MWS\Provider;

use Whoops\Run;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\JsonResponseHandler;
use MWS\Exception\Handler\LoggerHandler;
use MWS\Exception\Handler\ErrorPageHandler;

/**
 * MWS\Provider\ErrorHandler\ServiceProvider
 *
 * @package MWS\Provider\Environment
 */
class ErrorHandler extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'errorHandler';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared("{$this->serviceName}.loggerHandler", LoggerHandler::class);
        $this->di->setShared("{$this->serviceName}.prettyPageHandler", PrettyPageHandler::class);
        $this->di->setShared("{$this->serviceName}.errorPageHandler", ErrorPageHandler::class);

        $this->di->setShared(
            "{$this->serviceName}.jsonResponseHandler",
            function () {
                $handler = new JsonResponseHandler();
                $handler->setJsonApi(true);

                return $handler;
            }
        );

        $service = $this->serviceName;

        $this->di->setShared(
            $this->serviceName,
            function () use ($service) {
                $run  = new Run();

                if (env('APP_DEBUG', false)) {
                    $run->pushHandler(container("{$service}.prettyPageHandler"));
                } else {
                    $run->pushHandler(container("{$service}.errorPageHandler"));
                }

                $run->pushHandler(container("{$service}.loggerHandler"));

                return $run;
            }
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function boot()
    {
        container($this->serviceName)->register();
    }
}
