<?php

use Phalcon\Security;
use Phinx\Seed\AbstractSeed;

class AUserSeeder extends AbstractSeed
{

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $sec  = new Security();
        $data = [
            [

                'username' => 'admin',
                'password' => $sec->hash('admin')
            ],
            [

                'username' => 'user2fa',
                'password' => $sec->hash('user2fa')
            ],
            [
                'username' => 'userPGP',
                'password' => $sec->hash('userPGP')
            ],
            [
                'username' => 'userPin',
                'password' => $sec->hash('userPin')
            ],
            [
                'username' => 'userTest',
                'password' => $sec->hash('userTest')
            ],
            [
                'username' => 'userSecondTest',
                'password' => $sec->hash('userTest')
            ],
            [
                'username' => 'userThirdTest',
                'password' => $sec->hash('userTest')
            ],
            [
                'username' => 'userPgpTest',
                'password' => $sec->hash('userTest')
            ],
            [
                'username' => 'userPinTest',
                'password' => $sec->hash('userTest')
            ]

        ];

        $entity = $this->table('users');
        $entity->insert($data)->save();
    }
}
