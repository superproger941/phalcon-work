<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class PermissionModel
 * @package PhPermission\Models
 */
class PermissionModel extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(column="module_part_id", type="integer", length=32, nullable=false)
     */
    public $module_part_id;

    /**
     *
     * @var string
     * @Column(column="permission_code", type="string", nullable=false)
     */
    public $permission_code;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('permissions');

        $this->hasManyToMany(
            'id',
            'RolePermissionModel',
            'permission_id', 'role_id',
            'RoleModel',
            'id'
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'permissions';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'name',
            'module_part_id',
            'permission_code'
        ];
    }
}
