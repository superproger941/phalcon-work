echo "Prepare folder"
rm -rf dist
mkdir dist
cp -R scripts/serve dist/
cp -R scripts/docker-compose.yml dist/
cp -R scripts/setup.sh dist/

echo "Extract website"
docker pull registry.gitlab.com/upstream.llc/mozello/website:master
id=$(docker create registry.gitlab.com/upstream.llc/mozello/website:master)
echo "Created image $id. Move files..."
docker cp $id:/var/www dist/website
docker rm -v $id

echo "Extract module permission"
docker pull registry.gitlab.com/upstream.llc/mozello/modules/permission:master
id=$(docker create registry.gitlab.com/upstream.llc/mozello/modules/permission:master)
echo "Created image $id. Move files..."
docker cp $id:/var/www dist/module-permission
docker rm -v $id

echo "Extract module user"
docker pull registry.gitlab.com/upstream.llc/mozello/modules/user:master
id=$(docker create registry.gitlab.com/upstream.llc/mozello/modules/user:master)
echo "Created image $id. Move files..."
docker cp $id:/var/www dist/module-user
docker rm -v $id

echo "Create archive"
zip -r dist.zip dist > /dev/null
echo "DONE!"