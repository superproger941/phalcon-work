<?php

namespace PhPermission\Models;

use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use PhPermission\Repositories\RoleRepository;

/**
 * Class UserModel
 * @package PhPermission\Models
 */
class UserModel extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('users');

        $this->hasManyToMany(
            'id',
            UserAuthMethodModel::class,
            'user_id', 'auth_method_id',
            AuthMethodModel::class,
            'id',
            [
                'alias' => 'AuthMethods',
            ]
        );

        $this->belongsTo(
            'id',
            'PhPermission\Models\UserRoleModel',
            'user_id',
            [
                'alias' => 'UserRole',
            ]
        );

        $this->hasMany(
            'id',
            'PhPermission\Models\UserRoleModel',
            'user_id',
            [
                'alias' => 'UserRoles'
            ]
        );

        $this->belongsTo(
            'id',
            'PhPermission\Models\PgpModel',
            'user_id',
            [
                'alias' => 'Pgp',
            ]
        );

        $this->belongsTo(
            'id',
            'PhPermission\Models\MultifactorModel',
            'user_id',
            [
                'alias' => 'Multifactor',
            ]
        );

        $this->belongsTo(
            'id',
            'PhPermission\Models\PinModel',
            'user_id',
            [
                'alias' => 'Pin',
            ]
        );

        $this->hasManyToMany(
            'id',
            UserRoleModel::class,
            'user_id', 'role_id',
            RoleModel::class,
            'id',
            [
                'alias' => 'Roles',
            ]
        );
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'id'
        ];
    }

    /**
     * @param int $userId
     * @return object
     * @throws \Exception
     */
    public static function getUser(int $userId): object
    {
        $service = Di::getDefault();
        $db = $service->get('db');
        if (!$db->execute('INSERT INTO users (id) VALUES (' . $userId . ') ON CONFLICT DO NOTHING')) {
            throw new \Exception('couldn\'t insert');
        }
        return UserModel::query()
            ->where('id = :id:')
            ->bind(['id' => $userId])
            ->execute()
            ->getFirst();
    }

    /**
     * @param $roleId
     * @throws \Exception
     */
    public function setRole($roleId): void
    {
        $this->userRoles->delete();
        $role = RoleRepository::retrieveById($roleId);
        $this->roles = [$role];
    }

}
