<?php

namespace MWS\Models;

use Phalcon\Mvc\Model;

/**
 * Class AuthMethod
 * @package MWS\Models
 *
 */
class AuthMethod extends Model
{
    const AUTH_METHOD_DEFAULT = 'default';
    const AUTH_METHOD_2FA = '2fa';
    const AUTH_METHOD_PGP = 'pgp';
    const AUTH_METHOD_PIN = 'pin';

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=255, nullable=false)
     */
    public $name;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'MWS\Models\User', 'auth_method_id');
    }


    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'id',
            'name'
        ];
    }
}
