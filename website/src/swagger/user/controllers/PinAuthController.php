<?php

namespace MWS\Swagger\User\Controllers;

use MWS\Swagger\MainController;

/**
 * Class PinAuthController
 * @package MWS\Swagger\User\Controllers
 */
class PinAuthController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Do pin auth",
     *     operationId="doPin",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pgpAuth.doPin"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="pin",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="action_name",
     *                             type="string",
     *                             example="doLogin"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="success!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function doPin()
    {
        //auth_session_token
        //pin
        //action_name
    }
}