<?php

namespace PhUser\Controllers;

use Phalcon\Mvc\Controller;
use PhUser\Validation\GetQrCodeValidation;
use PhUser\Validation\User2faLoginValidation;
use PhUser\Validation\Verify2faValidation;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class MultiFactorAuthController
 * @package PhUser\Controllers
 */
class MultiFactorAuthController extends Controller
{
    /**
     * @return mixed
     */
    public function verify2fa()
    {
        $validator = new Verify2faValidation();
        $payload = $this->request->getJsonRawBody(true);
        $params = $payload['params'];
        $messages = $validator->validate($params);
        if (count($messages)) {
            return $this
                ->response
                ->setValidationError($messages);
        }
        //HERE WE HAVE AUTH USER
        $this->auth->check();
        $res = $this->permission->getGoogleAuthSecret($params['user_id']);
        if ($res->hasError()) {
            return $this
                ->response
                ->setPayloadError($res->getError()['message']);
        }
        if (!$this->multiFactor->checkCode($res->data['google_auth_secret'], $params['code'])) {
            return $this
                ->response
                ->setPayloadError('invalid code');
        }

        $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!'])
            ->setStatusCode(200);
    }

    /**
     * @return mixed
     * @throws \PhUser\Exceptions\UserNotFoundException
     */
    public function getQrCode()
    {
        $params = $this->params;
        $validator = new GetQrCodeValidation();
        if (!$this->validation->run($params->toArray(), $validator)) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        //HERE WE HAVE AUTH USER
        $this->auth->check();
        $qrCode = $this->multiFactor->generateMultiFactor();

        return $this
            ->response
            ->setPayloadSuccess(['code' => $qrCode]);
    }

    /**
     * @return mixed
     */
    public function do2fa()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new User2faLoginValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        try {
            $action = $this->multiFactor->run($params['auth_session_token'], $params['code'], $params['action_name'], PermissionService::AUTH_METHOD_2FA);
        } catch (\Exception $e) {
            return $this->response->setPayloadError($e->getMessage());
        }
        if (!empty($action)) {
            return $this
                ->response
                ->setPayloadSuccess([
                    'auth_session_token' => $params['auth_session_token'],
                    'action' => $action
                ]);
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'success!'
            ]);
    }
}