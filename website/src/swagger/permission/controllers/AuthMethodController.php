<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class AuthMethodController
 * @package MWS\Swagger\Permission\Controllers
 */
class AuthMethodController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Get auth methods' list",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.index"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                        @OA\Property(
     *                              property="auth_methods",
     *                              type="array",
     *                              @OA\Schema(
     *                                  type="array",
     *                                  @OA\Items(
     *                                      type="string",
     *                                      enum = {"2fa", "pgp", "pin"},
     *                                  )
     *                              )
     *                        )
     *                     )
     *                 )
     *              )
     *          )
     *      )
     *  )
     */
    public function index()
    {
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Check user' completed auth",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.show"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function show()
    {
        //user_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Set auth method for user id",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.setAuthMethod"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function setAuthMethod()
    {
        //user_id
        //auth_method_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Disable auth method",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.disableAuthMethod"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function disableAuthMethod()
    {
        //user_id
        //auth_method_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Unset auth method for user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.unsetAuthMethod"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function unsetAuthMethod()
    {
        //user_id
        //auth_method_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Set active status for auth method",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="authMethod.setAuthMethodActive"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function setAuthMethodActive()
    {
        //auth_method_id
        //active
    }

}