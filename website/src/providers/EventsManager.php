<?php

namespace MWS\Provider;

use Phalcon\Events\Manager;

/**
 * MWS\Provider\EventsManager\ServiceProvider
 *
 * @package MWS\Provider\EventManager
 */
class EventsManager extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'eventsManager';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $em = new Manager();
                $em->enablePriorities(true);

                return $em;
            }
        );
    }
}
