<?php

namespace PhUser\Components\AuthMethods;

use Phalcon\Mvc\ModelInterface;
use PhUser\Guard\AuthGuard;
use PhUser\User\IUserProvider;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

/**
 * Class MultiFactorAuthComponent
 * @package PhUser\Components\AuthMethods
 */
class MultiFactorAuthComponent extends AbstractAuthComponent
{
    /**
     * @var GoogleAuthenticator
     */
    private $google;

    /**
     * MultiFactorAuthComponent constructor.
     * @param AuthGuard $authGuard
     * @param IUserProvider $provider
     */
    public function __construct(AuthGuard $authGuard, IUserProvider $provider)
    {
        parent::__construct($authGuard, $provider);
        $this->google = new GoogleAuthenticator();
    }

    /**
     * @param string $sessionToken
     * @param string|null $str
     * @return ModelInterface
     * @throws \Exception
     */
    public function verify(string $sessionToken, string $str = null): ModelInterface
    {
        $user = $this->getUserBySessionToken($sessionToken);
        $res = $this->permission->getGoogleAuthSecret($user->id);
        if ($res->hasError()) {
            throw new \Exception($res->getError());
        }
        $googleAuthSecret = $res->data['google_auth_secret'];
        if (!$this->checkCode($googleAuthSecret, $str)) {
            throw new \Exception('Invalid 2fa code');
        }

        return $user;
    }

    /**
     * @return string
     * @throws \ParagonIE\Paseto\Exception\PasetoException
     */
    public function generateMultiFactor()
    {
        $secret = $this->google->generateSecret();
        $qrCode = GoogleQrUrl::generate(getenv('GOOGLE_ACCOUNT_NAME'), $secret);
        $user = $this->authGuard->user(true);
        $res = $this->permission->setGoogleAuthSecret($user->id, $secret);
        if ($res->hasError()) {
            throw new \Exception($res->getError()['message']);
        }

        return $qrCode;
    }

    public function checkCode(string $googleAuthSecret, string $code): bool
    {
        return $this->google->checkCode($googleAuthSecret, $code);
    }
}


