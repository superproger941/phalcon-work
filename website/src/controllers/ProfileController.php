<?php

namespace MWS\Controllers;

use MWS\Forms\Setup2faForm;
use MWS\Forms\SetupPgpForm;
use MWS\Forms\SetupPinForm;
use MWS\Forms\UpdateUserForm;
use MWS\Models\User;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class ProfileController
 * @package MWS\Controllers
 */
class ProfileController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    public function indexAction() {
        echo "profile index";
    }
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws \Exception
     */
    public function updateAction()
    {
        $form = new UpdateUserForm();
        $userId = $this->session->get('auth')['id'];
        $userInfo = $this->userComponent->getProfile($userId);
        if ($userInfo->hasError()) {
            $error = $userInfo->getError();
            $this->flash->error($error->message);
        }

        $user = new User();
        $userInfo = $userInfo->toArray();
        $form->bind($userInfo['data'], $user);
        $form->setEntity($user);
        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $userData = [
                'username' => $this->request->getPost('username', 'striptags'),
                'email' => $this->request->getPost('email', 'striptags'),
            ];
            $response = $this->userComponent->updateProfile($userId, $userData);


            if ($response->hasError()) {
                $error = $response->getError();
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }
            }
            $authMethod = $this->request->getPost('authMethod', 'striptags');
            $this->permissionComponent->setAuthMethod($userId, $authMethod);
            if ($response->hasError()) {
                $error = $response->getError();
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }
            }

            $roleId = $this->request->getPost('role', 'striptags');
            $response = $this->permissionComponent->setRole($userId, $roleId);
            if ($response->hasError()) {
                $error = $response->getError();
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }
            }
            $this->flash->success('Success!');

            return $this->response->redirect('/');
        }

        $this->view->form = $form;
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws \Exception
     */
    public function setup2faAction()
    {
        if ($this->userId() === null) {
            return $this->response->redirect('/');
        }
        $form = new Setup2faForm();

        $userId = $this->session->get('auth')['id'];
        $enabled = $this->permissionComponent->hasAuthMethod($this->userId(), PermissionService::AUTH_METHOD_2FA);

        $qrCode = null;
        if ($enabled === false && $this->request->isGet()) {
          $response = $this->userComponent->getQrCode($userId);
            if ($response->hasError()) {
                $error = $response->getError();
                $this->flash->error($error['message']);
            } else {
                $qrCode = $response->data['code'] ?? null;
            }
        }
        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $response = $this->userComponent->verify2faCode($userId, $this->request->getPost('code', 'striptags'));
            if (!$response->hasError()) {
                $response = $this->permissionComponent->setAuthMethod($userId, PermissionService::AUTH_METHOD_2FA);
                $enabled = $this->permissionComponent->hasAuthMethod($this->userId(), PermissionService::AUTH_METHOD_2FA);
                if (!$response->hasError()) {
                    $this->flash->success('Success!');
                    return $this->response->redirect('/profile/setup2fa');
                }
            }
            if ($response->hasError()) {
                $error = $response->getError();
                $this->flash->error($error['message']);
            }
        }
        $this->view->twoFaEnabled = $enabled;
        $this->view->qrCode = $qrCode;
        $this->view->form = $form;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setupPgpAction()
    {
        if ($this->userId() === null) {
            return $this->response->redirect('/');
        }
        $form = new SetupPgpForm();

        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $userData = [
                'publicKey' => $this->request->getPost('publicKey', 'striptags')
            ];
            $response = $this->permissionComponent->makePgp($this->userId(), $userData);
            if (!$response->hasError()) {
                $response = $this->permissionComponent->setAuthMethod($this->userId(), PermissionService::AUTH_METHOD_PGP);
                if (!$response->hasError()) {
                    $this->flash->success('PGP Key successfully set');
                    return $this->response->redirect('/profile/setupPgp');
                } else {
                    $this->flash->error($response->gerErrorMessage());
                }
            } else {
                $this->flash->error($response->gerErrorMessage());
            }
        }

        $this->view->pgpEnabled = $this->permissionComponent->hasAuthMethod($this->userId(), PermissionService::AUTH_METHOD_PGP);
        $this->view->form = $form;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function disablePgpAction()
    {
        $response = $this->permissionComponent->disableAuthMethod($this->userId(), 'pgp');
        if ($response === null) {
            $this->flash->error('PGP Key is not enabled');
        } elseif ($response->hasError()) {
            $this->flash->error($response->gerErrorMessage());
        } else {
            $this->flash->success('PGP Key was successfully unset');
        }
        return $this->response->redirect('/profile/setupPgp');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setupPinAction()
    {
        if ($this->userId() === null) {
            return $this->response->redirect('/');
        }
        $form = new SetupPinForm();
        $userId = $this->session->get('auth')['id'];
        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $userData = [
                'pin' => $this->request->getPost('code', 'striptags')
            ];
            $response = $this->permissionComponent->makePin($userId, $userData);
            if (!$response->hasError()) {
                $response = $this->permissionComponent->setAuthMethod($userId, PermissionService::AUTH_METHOD_PIN);
                if (!$response->hasError()) {
                    $this->flash->success('Pin successfully set');

                    return $this->response->redirect('/profile/setupPin');
                }
            }
        }

        $this->view->pinEnabled = $this->permissionComponent->hasAuthMethod($this->userId(), PermissionService::AUTH_METHOD_PIN);
        $this->view->form = $form;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function disablePinAction()
    {
        $response = $this->permissionComponent->disableAuthMethod($this->userId(), 'pin');
        if ($response === null) {
            $this->flash->error('PIN is not enabled');
        } elseif ($response->hasError()) {
            $this->flash->error($response->gerErrorMessage());
        } else {
            $this->flash->success('PIN was successfully unset');
        }
        return $this->response->redirect('/profile/setupPin');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function disableTwoFactoryAction()
    {
        $response = $this->permissionComponent->disableAuthMethod($this->userId(), '2fa');
        if ($response === null) {
            $this->flash->error('2FA is not enabled');
        } elseif ($response->hasError()) {
            $this->flash->error($response->gerErrorMessage());
        } else {
            $this->flash->success('2FA was successfully unset');
        }
        return $this->response->redirect('/profile/setup2fa');
    }
}
