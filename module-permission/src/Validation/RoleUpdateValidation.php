<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * Class RoleUpdateValidation
 * @package PhPermission\Validation
 */
class RoleUpdateValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfRoleId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field role_id is required',
            ]
        );
        $this->add('role_id', $presenceOfRoleId);
    }
}
