<?php

namespace PhPermission\Exceptions;

/**
 * Class UserNotFoundException
 * @package PhPermission\Exceptions
 */
class UserNotFoundException extends \Exception
{
}

