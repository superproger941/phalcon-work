<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreatePermissionTable extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('permissions', ['signed' => false]);
        $table
            ->addColumn('module_part_id', 'integer')
            ->addColumn('permission_code', Literal::from('varchar'), ['null' => true])
            ->create();

        $table = $this->table('modules', ['signed' => false]);
        $table
            ->addColumn('name', Literal::from('varchar'), ['null' => true])
            ->create();

        $table = $this->table('modules_parts', ['signed' => false]);
        $table
            ->addColumn('module_id', 'integer')
            ->addColumn('name', Literal::from('varchar'), ['null' => true])
            ->create();

        $table = $this->table('permissions');
        $table->addForeignKey('module_part_id', 'modules_parts');
        $table->save();

        $table = $this->table('modules_parts');
        $table->addForeignKey('module_id', 'modules');
        $table->save();

        $table = $this->table('roles_permissions', ['signed' => false]);
        $table
            ->addColumn('role_id', 'integer')
            ->addColumn('permission_id', 'integer')
            ->create();

        $table->changePrimaryKey(['role_id', 'permission_id']);
        $table->removeColumn('id');

        $table->addForeignKey('role_id', 'roles');
        $table->addForeignKey('permission_id', 'permissions');
        $table->save();
    }

}
