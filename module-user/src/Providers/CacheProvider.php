<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class CacheProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        /**
         * Cache
         * @description Phalcon - \Phalcon\Cache\Backend\Redis
         */
        $container->setShared('cache', function () {
            //Create a Data frontend and set a default lifetime to 1 day
            $frontend = new \Phalcon\Cache\Frontend\Data([
                'lifetime' => 86400,
                'prefix' => 'phalcon_user.cache.'
            ]);

            //Create the cache passing the connection
            $cache = new \Phalcon\Cache\Backend\Redis(
                $frontend,
                [
                    'host' => getenv('REDIS_HOST', 'redis'),
                    'port' => getenv('REDIS_PORT', 6379),
                    'persistent' => false,
                    'prefix' => 'phalcon_user.cache',
                    'index' => 0
                ]
            );

            return $cache;
        });
    }
}
