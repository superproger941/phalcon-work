<?php

namespace MWS\AfterRouteMiddleware;

use Phalcon\Mvc\DispatcherInterface;
use Phalcon\Mvc\User\Plugin;

/**
 * Class Event
 * @package MWS\AfterRouteMiddleware
 */
class Event extends Plugin
{
    /**
     * @param \Phalcon\Events\Event $event
     * @param DispatcherInterface $dispatcher
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function afterExecuteRoute(\Phalcon\Events\Event $event, DispatcherInterface $dispatcher, $data) : bool
    {
        $methodAnnotations = $this->annotations->getMethod(
            $dispatcher->getHandlerClass(),
            $dispatcher->getActiveMethod()
        );

        if (!$methodAnnotations->has("AfterRouteMiddleware")) {
            return true;
        }

        foreach ($methodAnnotations->getAll("AfterRouteMiddleware") as $annotation) {
            $class = $annotation->getArgument(0);

            $Middleware = new $class();

            if (!($Middleware instanceof MiddlewareInterface)) {
                throw new Exception(
                    "Not an after route middleware."
                );
            }

            $result = $Middleware->afterRoute();

            if ($result !== false) {
                return $result;
            }
        }

        return false;
    }
}
