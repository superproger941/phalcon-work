<?php

use Phalcon\Security;
use Phinx\Seed\AbstractSeed;

/**
 * Class PgpAndPinTestSeeder
 */
class PgpAndPinTestSeeder extends AbstractSeed
{

    public function run()
    {
        $sec  = new Security();
        $data = [
            [
                'user_id' => 5,
                'pgp_decrypt_hash' => 'testString',
            ],
            [
                'user_id' => 6,
                'pgp_decrypt_hash' => 'testUserSecondString',
            ],
            [
                'user_id' => 8,
                'pgp_decrypt_hash' => 'testPgpString',
            ]
        ];
        $entity = $this->table('user_pgp');
        $entity->insert($data)->save();

        $data = [
            [
                'user_id' => 5,
                'pin' => $sec->hash('userTest')
            ],
            [
                'user_id' => 6,
                'pin' => $sec->hash('userSecondTest')
            ],
            [
                'user_id' => 9,
                'pin' => $sec->hash('userPinTest')
            ]
        ];
        $entity = $this->table('user_pins');
        $entity->insert($data)->save();
    }

}
