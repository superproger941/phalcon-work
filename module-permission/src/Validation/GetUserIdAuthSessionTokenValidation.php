<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

/**
 * Class GetUserIdAuthSessionTokenValidation
 * @package PhPermission\Validation
 */
class GetUserIdAuthSessionTokenValidation extends Validation
{
    public function initialize()
    {
        $presenceOfAuthSessionToken = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth_session_token is required',
            ]
        );
        $this->add('auth_session_token', $presenceOfAuthSessionToken);
    }
}
