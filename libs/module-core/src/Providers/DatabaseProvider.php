<?php

declare(strict_types=1);

namespace PhCore\Providers;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;

class DatabaseProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'db',
            function () {
                return new \Phalcon\Db\Adapter\Pdo\Postgresql([
                    'host' => getenv('DB_HOST'),
                    'username' => getenv('POSTGRES_USER'),
                    'password' => getenv('POSTGRES_PASSWORD'),
                    'dbname' => getenv('POSTGRES_DB')
                ]);
            }
        );
    }
}
