<?php

define(APP_NAME, 'website');

use MWS\Bootstrap;

// Register the auto loader
require __DIR__.'/../bootstrap/autoloader.php';

$bootstrap = new Bootstrap();

echo $bootstrap->run();
